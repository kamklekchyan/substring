﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HomeWorkBet
{
    class Program
    {
        static void Main(string[] args)
        {
            string str = "adf";
            int a = FindAllSubstrings(str);
            string str1 = "helloworld";
            string substr = "owo";
            bool b = IsSubstring(str1, substr);
            Console.WriteLine(b);
            Console.ReadLine();
        }

        public static int FindAllSubstrings(string str)
        {
            int count = 0;
            for (int i = 0; i < str.Length; i++)
            {
                for (int j = 1; j <= str.Length - i; j++)
                {
                    Console.Write(str.Substring(i, j) + " ");
                    count++;
                }
                Console.WriteLine();
            }
            return count;
        }

        public static bool IsSubstring(string str1, string substr)
        {
            for (int i = 0; i < str1.Length; i++)
            {
                if (substr == str1.Substring(i, substr.Length)) return true;
            }
            return false;
        }
    }
}
